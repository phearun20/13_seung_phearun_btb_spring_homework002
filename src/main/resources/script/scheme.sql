CREATE TABLE customers(
    customerId SERIAL PRIMARY KEY ,
    customerName VARCHAR(50),
    customerAddress VARCHAR(50),
    customerPhone VARCHAR(50)
);

CREATE TABLE invoices(
    invoiceid SERIAL PRIMARY KEY ,
    invoicedate TIMESTAMP ,
    customerid INT REFERENCES customers(customerId)

);

CREATE TABLE products(
    productid SERIAL PRIMARY KEY ,
    productname VARCHAR(50) ,
    productprice VARCHAR(50)
);

CREATE TABLE invoicedetail(
    detailid SERIAL PRIMARY KEY ,
    invoiceid INT REFERENCES invoices(invoiceid) ON UPDATE CASCADE ON DELETE CASCADE ,
    productid INT REFERENCES products(productid) ON UPDATE CASCADE ON DELETE CASCADE
)