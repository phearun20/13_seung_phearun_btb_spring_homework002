package com.example.product.controller;

import com.example.product.model.entity.Invoice;
import com.example.product.model.reponse.InvoiceResponse;
import com.example.product.model.reponse.ProductResponse;
import com.example.product.model.request.InvoiceRequest;
import com.example.product.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get All Invoice")
    public ResponseEntity<List<Invoice>> getAllInvoice() {
        return ResponseEntity.ok(invoiceService.getAllInvoice());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Invoice BY Id")
    public ResponseEntity<Invoice> getInvoiceById(@PathVariable("id") Integer invoiceId) {
        return ResponseEntity.ok(invoiceService.getInvoiceById(invoiceId));

    }


    @DeleteMapping("/{id}")
    @Operation(summary = "Delete invoice by ID")
    public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId) {
        InvoiceResponse<String> response = null;
        if (invoiceService.deleteInvoiceById(invoiceId) == true) {
            response = InvoiceResponse.<String>builder()
                    .message("Delete successfully")
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }
    @PostMapping("/save")
    @Operation(summary = "Add new Invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse<Invoice> response = null;
        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if (storeInvoiceId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Add successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);

        }
return null;
    }
}
