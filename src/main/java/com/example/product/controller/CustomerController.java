package com.example.product.controller;

import com.example.product.model.entity.Customer;
import com.example.product.model.reponse.CustomerReponse;
import com.example.product.model.request.CustomerRequest;
import com.example.product.service.CumstomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.ibatis.annotations.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/vi/customer")

public class CustomerController {
    private final CumstomerService cumstomerService;

    public CustomerController(CumstomerService cumstomerService) {
        this.cumstomerService = cumstomerService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all customers")
    public ResponseEntity<CustomerReponse<List<Customer>>> getAllCumstomer(){
        CustomerReponse<List<Customer>> reponse = CustomerReponse.<List<Customer>>builder()
                .payload(cumstomerService.getAllCustomer())
                .message("Fetch Successfull")
                .success(true)
                .build();
        return ResponseEntity.ok(reponse);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get customer by iD")
    public ResponseEntity<CustomerReponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        CustomerReponse<Customer> response = null;
        if (cumstomerService.getCustomerById(customerId)!= null){
             response = CustomerReponse.<Customer>builder()
                    .payload(cumstomerService.getCustomerById(customerId))
                    .message("Success fetch data by Id")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = CustomerReponse.<Customer>builder()
                    .payload(cumstomerService.getCustomerById(customerId))
                    .message("Data not Found")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete customer by ID")
    public ResponseEntity<CustomerReponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomerReponse<String> response = null;
        if (cumstomerService.deleteCustomerById(customerId)==true){
            response = CustomerReponse.<String>builder()
                    .message("Delete successfully")
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);

    }


    @PostMapping
    @Operation(summary = "Save new customer")
    public ResponseEntity<CustomerReponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
        Integer storeCustomerId = cumstomerService.addNewCustomer(customerRequest);
        System.out.println(storeCustomerId);
        if (storeCustomerId != null) {
            CustomerReponse<Customer> response = CustomerReponse.<Customer>builder()
                    .payload(cumstomerService.getCustomerById(storeCustomerId))
                    .message("Add successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update customer by ID")
    public ResponseEntity<CustomerReponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId){
        CustomerReponse<Customer> response = null;
        Integer idCustomerUpdate = cumstomerService.updateCustomer(customerRequest, customerId);
        if (idCustomerUpdate !=null){
            response = CustomerReponse.<Customer>builder()
                    .payload(cumstomerService.getCustomerById(idCustomerUpdate))
                    .message("Update Successfully")
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}
