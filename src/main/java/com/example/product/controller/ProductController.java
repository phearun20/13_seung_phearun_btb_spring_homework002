package com.example.product.controller;

import com.example.product.model.entity.Customer;
import com.example.product.model.entity.Product;
import com.example.product.model.reponse.CustomerReponse;
import com.example.product.model.reponse.ProductResponse;
import com.example.product.model.request.CustomerRequest;
import com.example.product.model.request.ProductRequest;
import com.example.product.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/vi/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping("/all")
    @Operation(summary = "Get all product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProduct())
                .message("Fetch Successfull")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get product by iD")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        if (productService.getProductById(productId)!= null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Success fetch data by Id")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }else {
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Data not Found")
                    .success(false)
                    .build();
            return ResponseEntity.badRequest().body(response);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete PRODUCT by ID")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer productId) {
        ProductResponse<String> response = null;
        if (productService.deleteProductById(productId) == true) {
            response = ProductResponse.<String>builder()
                    .message("Delete successfully")
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

        @PostMapping("new")
        @Operation(summary = "Save new product")
        public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest ) {
            Integer storeProductId = productService.addNewProduct(productRequest);
            System.out.println(storeProductId);
            if (storeProductId != null) {
                ProductResponse<Product> response = ProductResponse.<Product>builder()
                        .payload(productService.getProductById(storeProductId))
                        .message("Add successfully")
                        .success(true)
                        .build();
                return ResponseEntity.ok(response);
            }
            return null;
        }

    @PutMapping("/{id}")
    @Operation(summary = "Update product by ID")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        Integer idProductUpdate = productService.updateProduct(productRequest, productId);
        if (idProductUpdate !=null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(idProductUpdate))
                    .message("Update Successfully")
                    .success(true)
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}








