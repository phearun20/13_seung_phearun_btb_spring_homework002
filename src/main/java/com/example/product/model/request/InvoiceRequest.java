package com.example.product.model.request;

import com.example.product.model.entity.Customer;
import com.example.product.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvoiceRequest {
    private Integer invoiceid;
    private Timestamp timestamp;
    private Customer customer;
    private  List<Integer> productsid;


}
