package com.example.product.model.reponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CustomerReponse<T>   {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private boolean success;



}
