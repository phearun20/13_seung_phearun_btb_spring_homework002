package com.example.product.repository;

import com.example.product.model.entity.Customer;
import com.example.product.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customers")
    List<Customer> findAllCustomer();
    @Select("SELECT * FROM customers WHERE customerid = #{customerid}")
    Customer getCustomerById(Integer cumstomerId);

    @Delete("DELETE FROM customers WHERE customerid = #{customerid}")
    boolean deleteCustomerById(@Param("customerid") Integer customerId);

    @Select("INSERT INTO customers (customername, customeraddress, customerphone) VALUES(#{request.customername}, #{request.customeraddress}, #{request.customerphone})" +
            "RETURNING customerid")
    Integer saveCustomer(@Param("request")CustomerRequest customerRequest);

    @Select("UPDATE customers " +
            "SET customername = #{request.customername}, " +
            "customeraddress = #{request.customeraddress}, " +
            "customerphone = #{request.customerphone} " +
            "WHERE customerid = #{customerId} " +
            "RETURNING customerid")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);

}
