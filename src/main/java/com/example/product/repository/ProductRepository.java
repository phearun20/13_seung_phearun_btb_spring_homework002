package com.example.product.repository;

import com.example.product.model.entity.Customer;
import com.example.product.model.entity.Product;
import com.example.product.model.request.CustomerRequest;
import com.example.product.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("SELECT p.productid, p.productname, p.productprice FROM invoicedetail ind " +
            "INNER JOIN products p ON p.productid = ind.productid " +
            "WHERE ind.invoiceid = #{invoiceId}")
    List<Product> getProductByInvocieId(Integer invoiceId);

    @Select("SELECT * FROM products")
    List<Product> findAllProduct();

    @Select("SELECT * FROM products WHERE productid = #{productid}")
    Product getProductById(Integer productId);

    @Delete("DELETE FROM products WHERE productid = #{productid}")
    boolean deleteProductById(@Param("productid") Integer productId);

    @Select("INSERT INTO products (productname, productprice) VALUES(#{request.productname}, #{request.productprice})" +
            "RETURNING productid")
    Integer saveProduct(@Param("request")ProductRequest productRequest);

    @Select("UPDATE products " +
            "SET productname = #{request.productname}, " +
            "productprice = #{request.productprice} " +
            "WHERE productid = #{productId} " +
            "RETURNING productid")
    Integer updateProduct(@Param("request")ProductRequest productRequest, Integer productId);


}
