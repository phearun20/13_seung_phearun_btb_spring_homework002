package com.example.product.repository;

import com.example.product.model.entity.Invoice;
import com.example.product.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoices")
    @Results(
            id = "invoiceMapper",
            value = {
                    @Result(property = "invoiceid", column = "invoiceid"),
                    @Result(property = "timestamp", column = "invoicedate"),
                    @Result(property = "customer", column = "customerid",
                            one = @One(select = "com.example.product.repository.CustomerRepository.getCustomerById")),
                    @Result(property = "productsid" , column = "invoiceid",
                            many = @Many(select = "com.example.product.repository.ProductRepository.getProductByInvocieId")
                    )

            }
    )
    List<Invoice> findAllInvoice();


    @Select("SELECT * FROM invoices WHERE invoiceid = #{invoiceid}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(Integer invoiceId);

    @Delete("DELETE FROM invoices WHERE invoiceid = #{invoiceid}")
    @ResultMap("invoiceMapper")
    boolean deleteInvoiceById(@Param("invoiceid") Integer invoiceId);

//    @Select("SELECT * FROM invoices WHERE invoiceid = #{invoiceid}")
//    @ResultMap("invoiceMapper")
//    Invoice getInvoiceById(Integer invoiceId);
    @Select("INSERT INTO invoices (timestamp, customerid) " +
            "VALUES (#{request.timestamp}, #{request.customerid}) " +
            "RETURNING invoiceid")
    Integer saveInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoicedetail (invoiceid, productid) " +
            "VALUES (#{invoiceId},#{productId})")
    Integer saveProductByInvoiceId(Integer invoiceId, Integer productId);


}
