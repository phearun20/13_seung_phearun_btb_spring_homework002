package com.example.product.service.ServiceImp;

import com.example.product.model.entity.Customer;
import com.example.product.model.request.CustomerRequest;
import com.example.product.repository.CustomerRepository;
import com.example.product.service.CumstomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CumstomerServiceImp implements CumstomerService {

    private final CustomerRepository customerRepository;

    public CumstomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId){
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.saveCustomer(customerRequest);
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdUpdate = customerRepository.updateCustomer(customerRequest, customerId);
        return customerIdUpdate;
    }


}
