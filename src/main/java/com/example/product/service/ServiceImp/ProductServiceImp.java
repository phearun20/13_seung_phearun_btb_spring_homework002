package com.example.product.service.ServiceImp;

import com.example.product.model.entity.Customer;
import com.example.product.model.entity.Product;
import com.example.product.model.request.CustomerRequest;
import com.example.product.model.request.ProductRequest;
import com.example.product.repository.ProductRepository;
import com.example.product.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        return productRepository.saveProduct(productRequest);
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdUpdate = productRepository.updateProduct(productRequest, productId);
        return productIdUpdate;
    }
}
