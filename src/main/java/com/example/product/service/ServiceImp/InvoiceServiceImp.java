package com.example.product.service.ServiceImp;

import com.example.product.model.entity.Invoice;
import com.example.product.model.request.InvoiceRequest;
import com.example.product.repository.InvoiceRepository;
import com.example.product.service.InvoiceService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice() {

        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);
        for (Integer invoiceId: invoiceRequest.getProductsid()
        ){
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId, invoiceId);
        }
        return storeInvoiceId;
    }
//
//    @Override
//    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
//        Integer storInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);
//        for (Integer productId: invoiceRequest.getProductsid()
//        ){
//            invoiceRepository.saveProductByInvoiceId(storInvoiceId,productId);
//        }
//        return storInvoiceId;
//    }
}
