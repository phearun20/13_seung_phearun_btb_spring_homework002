package com.example.product.service;
import com.example.product.model.entity.Customer;
import com.example.product.model.entity.Product;
import com.example.product.model.request.CustomerRequest;
import com.example.product.model.request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();
    Product getProductById(Integer productId);
    boolean deleteProductById(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);

    Integer updateProduct(ProductRequest productRequest, Integer productId);
}
