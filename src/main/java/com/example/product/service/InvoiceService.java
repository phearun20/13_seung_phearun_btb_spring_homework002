package com.example.product.service;

import com.example.product.model.entity.Invoice;
import com.example.product.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer invoiceId);

    boolean deleteInvoiceById(Integer invoiceId);
//
    Integer addNewInvoice(InvoiceRequest invoiceRequest);
}
