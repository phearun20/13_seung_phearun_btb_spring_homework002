package com.example.product.service;

import com.example.product.model.entity.Customer;
import com.example.product.model.request.CustomerRequest;
import io.swagger.v3.oas.models.security.SecurityScheme;

import java.util.List;

public interface CumstomerService {
    List<Customer> getAllCustomer();

    Customer getCustomerById(Integer customerId);
    boolean deleteCustomerById(Integer customerId);

    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);



}
